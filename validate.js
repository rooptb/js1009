function validateForm() {
  var mob = document.forms["form1"]["mobileDevices"].value;
  var uid = document.forms["form1"]["uid"].value;
  var gradYear = document.forms["form1"]["gradYear"].value;
  var birthYear = document.forms["form1"]["birthYear"].value;
  var cse =  document.forms["form1"]["cse"].value;
  var car = document.forms["form1"]["car"].value;
  var quote = document.forms["form1"]["quote"].value;
  var quoteCite = document.forms["form1"]["quoteCite"].value;


  if(isNaN(mob) || mob < 1 || mob > 20 ){
    alert("enter a valid number between 1-20 for mobile devices");
    return false;
  }

  if (uid.length < 2 || birthYear.length <2 || quote.length <2 || quoteCite.length <2) {
    alert("All text fields must be more than 2 characters");
    return false;
  }
  if(uid.trim().indexOf(' ') != -1){
    alert("Unique Id must be one word");
    return false;
  }
  var qCheck = quote.split(' ');
  if(qCheck.length < 4){
    alert("Quote must contain at least 4 words and no characters other than a-z,A-Z,0-9,.,-'");
    return false;
  }


  return true;
}
function showTime(){
  var d = new Date();
  document.getElementById("time").innerHTML = d.toString();
  setTimeout("showTime()",1000);
}
showTime();
